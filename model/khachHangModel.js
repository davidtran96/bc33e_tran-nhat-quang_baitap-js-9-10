function KhachHang(
  taikhoan,
  tenkh,
  email,
  password,
  date,
  luongcb,
  chucvu,
  giolam
) {
  this.taikhoan = taikhoan;
  this.tenkh = tenkh;
  this.email = email;
  this.password = password;
  this.date = date;
  this.luongcb = luongcb;
  this.chucvu = chucvu;
  this.giolam = giolam;

  this.ngaylam = function () {
    let today = new Date().toLocaleDateString();
    let diferrenctTime = Math.floor(
      (Date.parse(today) - Date.parse(this.date)) / 86400000
    );
    // let diferrenctTime = today - this.date;
    return diferrenctTime;
  };
  this.tongluong = function () {
    if (this.chucvu == "Sếp") {
      return this.luongcb * 3;
    } else if (this.chucvu == "Trưởng phòng") {
      return this.luongcb * 2;
    } else if (this.chucvu == "Nhân viên") {
      return this.luongcb * 1;
    }
  };
  this.xeploai = function () {
    let giolam = parseInt(this.giolam);
    if (giolam >= 192) {
      return "Nhân viên xuất sắc";
    } else if (giolam >= 176) {
      return "Nhân viên giỏi";
    } else if (giolam >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}

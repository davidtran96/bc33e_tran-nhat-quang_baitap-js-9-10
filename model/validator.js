let validator = {
  kiemTraRong: function (valueInput, idError, message) {
    if (valueInput == "") {
      document.getElementById(idError).innerHTML = message;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  },
  kiemTaiKhoanKH: function (taikhoan, dskh) {
    let index = dskh.findIndex((kh) => {
      return kh.ma == taikhoan;
    });
    if (index !== -1) {
      document.getElementById("tbTKNV").innerHTML = "Tài khoản đã tồn tại";
      return false;
    } else {
      document.getElementById("tbTKNV").innerHTML = "";
      return true;
    }
  },
  kiemTraDoDai: function (valueInput, idError, min, max) {
    var inputLength = valueInput.length;
    if (inputLength > max || inputLength < min) {
      document.getElementById(
        idError
      ).innerHTML = `Độ dài nhập vào cần từ ${min}-${max} ký tự`;
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  },
  kiemTraChuoiSo: function (valueInput, idError) {
    let regex = /^[0-9]+$/;
    if (regex.test(valueInput)) {
      document.getElementById(idError).innerHTML = "";
      return true;
    } else {
      document.getElementById(idError).innerHTML =
        "Trường này chỉ được nhập số";
      return false;
    }
  },
};

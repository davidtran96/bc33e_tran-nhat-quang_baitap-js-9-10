var dskh = [];
const DSKH = "DSKH";

let dskhLocalStorage = localStorage.getItem(DSKH);
if (JSON.parse(dskhLocalStorage)) {
  let data = JSON.parse(dskhLocalStorage);
  for (let i = 0; i < data.length; i++) {
    let current = data[i];
    let kh = new KhachHang(
      current.taikhoan,
      current.tenkh,
      current.email,
      current.password,
      current.date,
      current.luongcb,
      current.chucvu,
      current.giolam
    );
    dskh.push(kh);
  }
  renderDanhSachKhachHang(dskh);
}
function saveLocalStorage() {
  let dskhJson = JSON.stringify(dskh);
  localStorage.setItem(DSKH, dskhJson);
}

function themNV() {
  let newKH = layThongTinTuForm();
  let isValid =
    validator.kiemTraRong(
      newKH.taikhoan,
      "tbTKNV",
      "Tài khoản không được rỗng"
    ) &&
    validator.kiemTaiKhoanKH(newKH.taikhoan, dskh) &&
    validator.kiemTraDoDai(newKH.taikhoan, "tbTKNV", 6, 8) &&
    validator.kiemTraChuoiSo(newKH.taikhoan, "tbTKNV");
  isValid =
    isValid &
    validator.kiemTraRong(
      newKH.tenkh,
      "tbTen",
      "Tên khách hàng không được rỗng"
    ) &
    validator.kiemTraRong(
      newKH.email,
      "tbEmail",
      "Email khách hàng không được rỗng"
    ) &
    validator.kiemTraRong(
      newKH.password,
      "tbMatKhau",
      "Mật khẩu không được rỗng"
    ) &
    validator.kiemTraRong(newKH.date, "tbNgay", "Ngày không được rỗng") &
    validator.kiemTraRong(
      newKH.luongcb,
      "tbLuongCB",
      "Lương cơ bản không được rỗng"
    ) &
    validator.kiemTraRong(newKH.chucvu, "tbChucVu", "Chức vụ không được rỗng") &
    validator.kiemTraRong(newKH.giolam, "tbGiolam", "Giờ làm không được rỗng");
  if (isValid) {
    dskh.push(newKH);
    let dskhJson = JSON.stringify(dskh);
    localStorage.setItem(DSKH, dskhJson);
    renderDanhSachKhachHang(dskh);
  }
}

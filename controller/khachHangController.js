function getEl(x) {
  return document.getElementById(x);
}

function layThongTinTuForm() {
  let taikhoan = getEl("tknv").value.trim(),
    tenkh = getEl("name").value.trim(),
    email = getEl("email").value.trim(),
    password = getEl("password").value.trim(),
    date = getEl("datepicker").value,
    luongcb = getEl("luongCB").value.trim(),
    chucvu = getEl("chucvu").value.trim(),
    giolam = getEl("gioLam").value.trim();

  let kh = new KhachHang(
    taikhoan,
    tenkh,
    email,
    password,
    date,
    luongcb,
    chucvu,
    giolam
  );
  return kh;
}

function renderDanhSachKhachHang(list){
  let contentHTML = "";
  list.forEach(function (item) {
    let content = `<tr>
    <td>${item.taikhoan}</td>
    <td>${item.tenkh}</td>
    <td>${item.email}</td>
    <td>${item.ngaylam()}</td>
    <td>${item.chucvu}</td>
    <td>${item.tongluong()}</td>
    <td>${item.xeploai()}</td>
    </tr>
    `;
    contentHTML += content;
  });
  getEl("tableDanhSach").innerHTML = contentHTML;
}
